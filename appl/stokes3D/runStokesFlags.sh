#!/bin/bash
gitter=50_50_10_SC
pathToSave=/home/felix/Dokumente/Aktuelles_U/Masterarbeit/Postprocessing/DumuxOutput/
gradPressure_array='2e3 2e4 2e5 2e6 2e7 2e8'
lengthGeometry=67.5742e-6
rm -f *.vtu
rm -f *.pvd
rm -f *.console

for gradPressure in $gradPressure_array; do

nameGradPressure=$gradPressure/
nameGeometry=$gitter\.raw/
nameVtkOutput=$gitter\_$gradPressure/
nameLengthGeometry=$lengthGeometry/


nameConsoleOutput=$gitter\_$gradPressure.console
nameDirectory=$gitter/$nameVtkOutput

mkdir -p $nameDirectory

sed  -i "s/GradP.*$/GradP = ${nameGradPressure}" stokes_flags.input
sed  -i "s/GeometryFile.*$/GeometryFile = ${nameGeometry}" stokes_flags.input
sed  -i "s/Name.*$/Name = ${nameVtkOutput}" stokes_flags.input

sed  -i "s/Positions0.*$/Positions0 = 0.00 ${nameLengthGeometry}" stokes_flags.input
sed  -i "s/Length.*$/Length = ${nameLengthGeometry}" stokes_flags.input
sed  -i "s/Positions1.*$/Positions1 = 0.00 ${nameLengthGeometry}" stokes_flags.input
sed  -i "s/Width.*$/Width = ${nameLengthGeometry}" stokes_flags.input

echo

echo 'Start the simulation:'
echo $gitter\_$gradPressure
./test_stokes_perm_3d_flags stokes_flags.input >$nameConsoleOutput

sed  -i "s/velocity_component\s[(]m[/]s[)]/velocity/g" *.vtu
mv *.vtu $nameDirectory
mv *.pvd $nameDirectory
mv *.console $nameDirectory

mkdir -p $pathToSave$gitter
cp -r $nameDirectory $pathToSave$gitter
echo 'Simulation done moved into directory:'
echo $nameDirectory
echo 'Data copied into directory:'
echo $pathToSave
done


