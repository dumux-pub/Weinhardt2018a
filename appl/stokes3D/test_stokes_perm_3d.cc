// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
 #include <config.h>

 #include <ctime>
 #include <iostream>
#include <fstream>

 #include <dune/common/parallel/mpihelper.hh>
 #include <dune/common/timer.hh>
 #include <dune/grid/io/file/dgfparser/dgfexception.hh>
 #include <dune/grid/io/file/vtk.hh>
 #include <dune/istl/io.hh>


#include "stokesperm3dproblem.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/valgrind.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/staggeredfvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/io/staggeredvtkoutputmodule.hh>
#include <dumux/freeflow/navierstokes/staggered/fluxoversurface.hh>


#include <dune/common/fvector.hh>
#include <dumux/io/gridcreator.hh>
#include <dumux/io/subgridgridcreator.hh>
#include <dumux/discretization/methods.hh>

#include <dumux/io/gnuplotinterface.hh>

/*!
 * \brief A method providing an () operator in order to select elements for a subgrid.
 */

bool IsInCircleAroundPos (double x, double y, double Pos_x, double Pos_y, double radius_, double eps_)
        {
            return std::sqrt((x - Pos_x)*(x - Pos_x) + (y - Pos_y)*(y - Pos_y)) < radius_ - eps_;
            
        }

class GeometryForSubgrid
{
public:
    double corner_bot_left_x_;
    double corner_bot_left_y_;
    double corner_top_left_x_;
    double corner_top_left_y_;
    double corner_bot_right_x_;
    double corner_bot_right_y_;
    double corner_top_right_x_;
    double corner_top_right_y_;
    double mid_point_x_;
    double mid_point_y_;
    double radius_;
    bool sc_;
    GeometryForSubgrid(double length, double width, double depth, double porosity, bool sc)
    {
        sc_ = sc;
        corner_bot_left_x_ = 0;
        corner_top_left_x_ = 0;
        
        corner_bot_right_x_ = length;
        corner_top_right_x_ = length;
        
        
        corner_bot_left_y_ = 0;
        corner_bot_right_y_ = 0;
        
        corner_top_left_y_ = width;
        corner_top_right_y_ = width;
        
        mid_point_x_ = length / 2.;
        mid_point_y_ = width / 2.;
        
        areaTotal_ = (length * width);
        
        volumeTotal_ = areaTotal_ * depth;
        
        radius_ = std::sqrt( ( (1-porosity)* areaTotal_ ) / (2* M_PI) );
        
        if(sc_)
        {
            radius_ = std::sqrt( ( (1-porosity)* areaTotal_ ) / M_PI );
        }
        
    }
    
    double GetRadius()
    {
        return radius_;
    }
    
private:
    double areaTotal_;
    double volumeTotal_;
    double porosity_;
};

template< class GeometryForSubgrid>
class PoresSelector
{
public:
    PoresSelector(GeometryForSubgrid geometryForSubgrid) : geomData_ (geometryForSubgrid){}

    //! Select all elements within a circle around a center point.
    int operator() (const auto& element) const
    {
        const auto x = element.geometry().center()[0];
        const auto y = element.geometry().center()[1];
        return (          true
                        &&!IsInCircleAroundPos (x, y,geomData_.corner_bot_left_x_, geomData_.corner_bot_left_y_, geomData_.radius_, eps_)                //circle bot left
                        &&!IsInCircleAroundPos (x, y,geomData_.corner_top_left_x_, geomData_.corner_top_left_y_, geomData_.radius_, eps_)        //circle top left
                        &&!IsInCircleAroundPos (x, y,geomData_.corner_bot_right_x_, geomData_.corner_bot_right_y_, geomData_.radius_, eps_)    //circle bot right
                        &&!IsInCircleAroundPos (x, y,geomData_.corner_top_right_x_, geomData_.corner_top_right_y_, geomData_.radius_, eps_)    //circle top right
                        &&(!IsInCircleAroundPos (x, y,geomData_.mid_point_x_, geomData_.mid_point_y_, geomData_.radius_, eps_)||geomData_.sc_)     //circle mid
        );
    }
private:
    //GlobalPosition center_;
    GeometryForSubgrid geomData_;
    double eps_ = 1e-13;
    bool returnValue;
};

/*!
 * \brief Provides an interface for customizing error messages associated with
 *        reading in parameters.
 *
 * \param progName  The name of the program, that was tried to be started.
 * \param errorMsg  The error message that was issued by the start function.
 *                  Comprises the thing that went wrong and a general help message.
 */
void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe list of mandatory arguments for this program is:\n"
                                        "\t-TimeManager.TEnd               End of the simulation [s] \n"
                                        "\t-TimeManager.DtInitial          Initial timestep size [s] \n"
                                        "\t-Grid.File                      Name of the file containing the grid \n"
                                        "\t                                definition in DGF format\n"
                                        "\t-SpatialParams.LensLowerLeftX   x-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensLowerLeftY   y-coordinate of the lower left corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightX  x-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.LensUpperRightY  y-coordinate of the upper right corner of the lens [m] \n"
                                        "\t-SpatialParams.Permeability     Permeability of the domain [m^2] \n"
                                        "\t-SpatialParams.PermeabilityLens Permeability of the lens [m^2] \n";

        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = TTAG(ThreeDPermTestTypeTag);

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv, usage);

    constexpr int dim = 3;
    using GlobalPosition = Dune::FieldVector<double, dim>;

    // try to create a grid (from the given grid file or the input file)
    using HostGrid = typename GET_PROP(TypeTag, Grid)::HostGrid;
    using HostGridCreator = GridCreatorImpl<HostGrid, DiscretizationMethod::staggered>;
    HostGridCreator::makeGrid();
    
    
    using GridCreator = typename GET_PROP_TYPE(TypeTag, GridCreator);
    
    //const GlobalPosition step{getParam<double>("Problem.Step0"), getParam<double>("Problem.Step1")};
    const GlobalPosition step{0., 0.};
    
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    
    //read Properties for geometrie from input file and set up a geometrie for the subgrid
    Scalar porosity_ = getParam<Scalar>("SolidMatrix.Porosity");
    Scalar domainLength_ = getParam<Scalar>("Grid.Length");
    Scalar domainWidth_ = getParam<Scalar>("Grid.Width");
    Scalar domainDepth_ = getParam<Scalar>("Grid.Depth");
    bool simpleCubing_ =  getParam<bool>("SolidMatrix.SimpleCubing");
    
    GeometryForSubgrid geomForSubgrid_(domainLength_, domainWidth_, domainDepth_, porosity_, simpleCubing_); 
    
    //PoresSelector<GlobalPosition, Scalar> subgridSelectorPlaneBot(step,  radius);
    PoresSelector<GeometryForSubgrid> subgridSelectorPlaneBot(geomForSubgrid_);

    auto botGrid = GridCreator::makeGrid(HostGridCreator::grid(), subgridSelectorPlaneBot);
    botGrid->loadBalance();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    //const auto& leafGridView = GridCreator::grid().leafGridView();
    const auto& leafGridView = botGrid->leafGridView();

    // create the finite volume grid geometry
    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    auto fvGridGeometry = std::make_shared<FVGridGeometry>(leafGridView);
    fvGridGeometry->update();

    // the problem (boundary conditions)
    
    using Problem = typename GET_PROP_TYPE(TypeTag, Problem);
    auto problem = std::make_shared<Problem>(fvGridGeometry);

    // the solution vector
    using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
    static constexpr auto cellCenterIdx = FVGridGeometry::cellCenterIdx();
    static constexpr auto faceIdx = FVGridGeometry::faceIdx();
    const auto numDofsCellCenter = leafGridView.size(0);
    const auto numDofsFace = leafGridView.size(1);
    SolutionVector x;
    x[cellCenterIdx].resize(numDofsCellCenter);
    x[faceIdx].resize(numDofsFace);

    // the grid variables
    using GridVariables = typename GET_PROP_TYPE(TypeTag, GridVariables);
    auto gridVariables = std::make_shared<GridVariables>(problem, fvGridGeometry);
    gridVariables->init(x);
    

    // intialize the vtk output module
    using VtkOutputFields = typename GET_PROP_TYPE(TypeTag, VtkOutputFields);
    StaggeredVtkOutputModule<TypeTag> vtkWriter(*problem, *fvGridGeometry, *gridVariables, x, problem->name());
    VtkOutputFields::init(vtkWriter); //!< Add model specific output fields
    vtkWriter.write(0.0);
  

    // the assembler with time loop for instationary problem
    using Assembler = StaggeredFVAssembler<TypeTag, DiffMethod::numeric>;
    auto assembler = std::make_shared<Assembler>(problem, fvGridGeometry, gridVariables);

    // the linear solver
    using LinearSolver = Dumux::UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<Assembler, LinearSolver>;
    NewtonSolver nonLinearSolver(assembler, linearSolver);

    // set up two planes over which fluxes are calculated
    FluxOverSurface<TypeTag> flux(*assembler, x);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using GlobalPosition = Dune::FieldVector<Scalar, GridView::dimensionworld>;
    
    const Scalar xMin = fvGridGeometry->bBoxMin()[0];
    const Scalar xMax = fvGridGeometry->bBoxMax()[0];
    const Scalar yMin = fvGridGeometry->bBoxMin()[1];
    const Scalar yMax = fvGridGeometry->bBoxMax()[1];
#if DIM_3D
    const Scalar zMin = fvGridGeometry->bBoxMin()[2];
    const Scalar zMax = fvGridGeometry->bBoxMax()[2];
#endif

    // The first plane shall be placed at the middle of the channel.
    // If we have an odd number of cells in x-direction, there would not be any cell faces
    // at the postion of the plane (which is required for the flux calculation).
    // In this case, we add half a cell-width to the x-position in order to make sure that
    // the cell faces lie on the plane. This assumes a regular cartesian grid.
    // The second plane is placed at the outlet of the channel.
#if DIM_3D
    const auto p0inlet = GlobalPosition{xMin, yMin, zMin};
    const auto p1inlet = GlobalPosition{xMin, yMax, zMin};
    const auto p2inlet = GlobalPosition{xMin, yMin, zMax};
    const auto p3inlet = GlobalPosition{xMin, yMax, zMax};
    flux.addSurface("inlet", p0inlet, p1inlet, p2inlet, p3inlet);
#else
    const auto p0inlet = GlobalPosition{xMin, yMin};
    const auto p1inlet = GlobalPosition{xMin, yMax};
    flux.addSurface("inlet", p0inlet, p1inlet);
#endif

    const Scalar planePosMiddleX = xMin + 0.5*(xMax - xMin);
    const int numCellsX = getParam<std::vector<int>>("Grid.Cells")[0];
    const Scalar offsetX = (numCellsX % 2 == 0) ? 0.0 : 0.5*((xMax - xMin) / numCellsX);

#if DIM_3D
    const auto p0middle = GlobalPosition{planePosMiddleX + offsetX, yMin, zMin};
    const auto p1middle = GlobalPosition{planePosMiddleX + offsetX, yMax, zMin};
    const auto p2middle = GlobalPosition{planePosMiddleX + offsetX, yMin, zMax};
    const auto p3middle = GlobalPosition{planePosMiddleX + offsetX, yMax, zMax};
    flux.addSurface("middle", p0middle, p1middle, p2middle, p3middle);
#else
    const auto p0middle = GlobalPosition{planePosMiddleX + offsetX, yMin};
    const auto p1middle = GlobalPosition{planePosMiddleX + offsetX, yMax};
flux.addSurface("middle", p0middle, p1middle);
#endif

    // The second plane is placed at the outlet of the channel.
#if DIM_3D
    const auto p0outlet = GlobalPosition{xMax, yMin, zMin};
    const auto p1outlet = GlobalPosition{xMax, yMax, zMin};
    const auto p2outlet = GlobalPosition{xMax, yMin, zMax};
    const auto p3outlet = GlobalPosition{xMax, yMax, zMax};
    flux.addSurface("outlet", p0outlet, p1outlet, p2outlet, p3outlet);
#else
    const auto p0outlet = GlobalPosition{xMax, yMin};
    const auto p1outlet = GlobalPosition{xMax, yMax};
    flux.addSurface("outlet", p0outlet, p1outlet);
#endif

    // linearize & solve
    Dune::Timer timer;
    nonLinearSolver.solve(x);
    // write vtk output
    vtkWriter.write(1.0);

    // calculate and print mass fluxes over the planes
    flux.calculateMassOrMoleFluxes();
    if(GET_PROP_TYPE(TypeTag, ModelTraits)::enableEnergyBalance())
    {
        std::cout << "mass / energy flux at inlet is: " << flux.netFlux("inlet") << std::endl;
        std::cout << "mass / energy flux at middle is: " << flux.netFlux("middle") << std::endl;
        std::cout << "mass / energy flux at outlet is: " << flux.netFlux("outlet") << std::endl;
    }
    else
    {
        std::cout << "mass flux at inlet is: " << flux.netFlux("inlet") << std::endl;
        std::cout << "mass flux at middle is: " << flux.netFlux("middle") << std::endl;
        std::cout << "mass flux at outlet is: " << flux.netFlux("outlet") << std::endl;
    }

    // calculate and print volume fluxes over the planes
    flux.calculateVolumeFluxes();
    std::cout << "volume flux at inlet is: " << flux.netFlux("inlet")[0] << std::endl;
    std::cout << "volume flux at middle is: " << flux.netFlux("middle")[0] << std::endl;
    std::cout << "volume flux at outlet is: " << flux.netFlux("outlet")[0] << std::endl;


    timer.stop();

    std::cout << "analyticalFlux: " << problem->analyticalFlux() << std::endl;

    const auto& comm = Dune::MPIHelper::getCollectiveCommunication();
    std::cout << "Simulation took " << timer.elapsed() << " seconds on "
              << comm.size() << " processes.\n"
              << "The cumulative CPU time was " << timer.elapsed()*comm.size() << " seconds.\n";


    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
} // end main
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
