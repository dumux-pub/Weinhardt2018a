// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Channel flow test for the staggered grid (Navier-)Stokes model.
 *
 * The channel is either modeled in 3D or in 2D, using an additional wall friction term
 * to mimic the 3D behavior of the flow.
 *
 */
#ifndef DUMUX_3D_PERM_PROBLEM_HH
#define DUMUX_3D_PERM_PROBLEM_HH

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/navierstokes/problem.hh>
#include <dumux/discretization/staggered/freeflow/properties.hh>
#include <dumux/freeflow/navierstokes/model.hh>
#include <dune/common/float_cmp.hh>
#include <dumux/io/subgridgridcreator.hh>
#ifndef DIM_3D
#define DIM_3D 0
#endif

namespace Dumux
{


template <class TypeTag>
class ThreeDPermTestProblem;

namespace Properties
{
NEW_TYPE_TAG(ThreeDPermTestTypeTag, INHERITS_FROM(StaggeredFreeFlowModel, NavierStokes));

// the fluid system
SET_PROP(ThreeDPermTestTypeTag, FluidSystem)
{
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using type = FluidSystems::OnePLiquid<Scalar, Components::Constant<1, Scalar> >;
};


// Set the grid type
SET_PROP(ThreeDPermTestTypeTag, Grid)
{
private:
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    static constexpr auto dim = 3;

public:
    // using HostGrid = Dune::YaspGrid<dim>;
    using HostGrid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<Scalar, dim> >;
    using type = Dune::SubGrid<dim, HostGrid>;
};

SET_PROP(ThreeDPermTestTypeTag, GridCreator)
{
private:
    using HostGrid = typename GET_PROP(TypeTag, Grid)::HostGrid;

public:
    using type = Dumux::SubgridGridCreator<HostGrid>;
};
/*
// Set the grid type
#if DIM_3D
SET_TYPE_PROP(ThreeDPermTestTypeTag, Grid, Dune::YaspGrid<3>);
#else
SET_TYPE_PROP(ThreeDPermTestTypeTag, Grid, Dune::YaspGrid<2>);
#endif

*/

// Set the problem property
SET_TYPE_PROP(ThreeDPermTestTypeTag, Problem, ThreeDPermTestProblem<TypeTag> );

SET_BOOL_PROP(ThreeDPermTestTypeTag, EnableFVGridGeometryCache, true);
SET_BOOL_PROP(ThreeDPermTestTypeTag, EnableGridFluxVariablesCache, true);
SET_BOOL_PROP(ThreeDPermTestTypeTag, EnableGridVolumeVariablesCache, true);
}

/*!
 * \brief  Test problem for the one-phase model:
   \todo doc me!
 */
template <class TypeTag>
class ThreeDPermTestProblem : public NavierStokesProblem<TypeTag>
{
    using ParentType = NavierStokesProblem<TypeTag>;

    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);

    using Indices = typename GET_PROP_TYPE(TypeTag, ModelTraits)::Indices;
    using BoundaryTypes = typename GET_PROP_TYPE(TypeTag, BoundaryTypes);
    using Element = typename GridView::template Codim<0>::Entity;

    using FVGridGeometry = typename GET_PROP_TYPE(TypeTag, FVGridGeometry);
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;
    using GlobalPosition = Dune::FieldVector<Scalar, dimWorld>;

    using CellCenterPrimaryVariables = typename GET_PROP_TYPE(TypeTag, CellCenterPrimaryVariables);
    using FacePrimaryVariables = typename GET_PROP_TYPE(TypeTag, FacePrimaryVariables);

    using PrimaryVariables = typename GET_PROP_TYPE(TypeTag, PrimaryVariables);
    using NumEqVector = typename GET_PROP_TYPE(TypeTag, NumEqVector);

    static constexpr bool enablePseudoThreeDWallFriction = !DIM_3D;

public:
    ThreeDPermTestProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry), eps_(1e-6)
    {
        
        // Read in all the Parameter from input file
        gradP_ = getParam<Scalar>("Problem.GradP");
        deltaP_ = getParam<Scalar>("Problem.GradP")*this->fvGridGeometry().bBoxMax()[0];
        pressureInitial_ = getParam<Scalar>("Problem.PressureInitial");
        linearPressure_ = getParam<bool>("Problem.SetLinearPressure");
        height_ = getParam<Scalar>("Problem.Height");
        rho_ = getParam<Scalar>("Component.LiquidDensity");
        nu_ = getParam<Scalar>("Component.LiquidKinematicViscosity");
        
        numDofs_x_ = getParam<Scalar>("Grid.Cells0");
        numDofs_y_ = getParam<Scalar>("Grid.Cells1");
        numDofs_z_ = getParam<Scalar>("Grid.Cells2");
        
        voxelSize_x_ = this->fvGridGeometry().bBoxMax()[0] / numDofs_x_;
        voxelSize_y_ = this->fvGridGeometry().bBoxMax()[1] / numDofs_y_;
        voxelSize_z_ = this->fvGridGeometry().bBoxMax()[2] / numDofs_z_;
        
        Scalar numDofsAll_ = numDofs_x_*numDofs_y_*numDofs_z_;
        auto numDofsActual = this->fvGridGeometry().numCellCenterDofs();
        porosity_ =  static_cast<Scalar>(numDofsActual)  /   (numDofsAll_);
        
        areaTotal_ = (this->fvGridGeometry().bBoxMax()[0] - this->fvGridGeometry().bBoxMin()[0])
                        * (this->fvGridGeometry().bBoxMax()[1] - this->fvGridGeometry().bBoxMin()[1]);
        
        volumeTotal_ = areaTotal_ * (this->fvGridGeometry().bBoxMax()[2] - this->fvGridGeometry().bBoxMin()[2]);
        
        
        //Prints information to paramaeter to console 
        std::cout << "\nProperties of grid:"<<std::endl;
        std::cout << "  x length : "<< this->fvGridGeometry().bBoxMax()[0]<< " [m] " << std::endl;
        std::cout << "  y length : "<< this->fvGridGeometry().bBoxMax()[1]<< " [m] " << std::endl;
        std::cout << "  z length : "<< this->fvGridGeometry().bBoxMax()[2]<< " [m] " << std::endl;
        std::cout << std::endl;
        std::cout << "  voxel size x:  "<< voxelSize_x_ << " [m]    y: " << voxelSize_y_<< " [m]    z:  "<<voxelSize_z_ << " [m]"<< std::endl;
        std::cout << "  voxel volume: " << voxelSize_x_ * voxelSize_y_ * voxelSize_z_ << " [m³]" <<std::endl;
        std::cout << std::endl;
        std::cout << "  numDofsActual: " << numDofsActual<<std::endl;
        std::cout << "  numDofsAll: " << numDofsAll_<<std::endl;
        std::cout << "  porosity: " << porosity_ << std::endl;
        std::cout << std::endl;
        std::cout << "  Volume Pores : " << volumeTotal_*porosity_ << std::endl;
        std::cout << "  Volumen Total : " << volumeTotal_ << " [m³] "<<std::endl;
        
        std::cout<<"\nPressure Parameters:"<<std::endl;
        std::cout<< "  delta p : "<<deltaP_ << " [Pa] " << std::endl;
        std::cout << "  gradient p : " << gradP_<< " [Pa/m]" << std::endl;
        std::cout << "  presssure is set to: " << pressureInitial_ << " [Pa]" << std::endl;    
        if(linearPressure_)
            std::cout << "  Initial Pressure is set linear " << std::endl;
        else 
            std::cout << "  Initial pressure is set equally " << std::endl;
        
        std::cout << std::endl;
        
        if(dim == 3 && !Dune::FloatCmp::eq(height_, this->fvGridGeometry().bBoxMax()[2]))
            DUNE_THROW(Dune::InvalidStateException, "z-dimension must equal height");

        if(enablePseudoThreeDWallFriction)
            extrusionFactor_ = 2.0/3.0 * height_;
        else
            extrusionFactor_ = 1.0;
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Return the temperature within the domain in [K].
     *
     * This problem assumes a temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    { return 273.15 + 10; } // 10C


    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume face.
     */
    using ParentType::source;
    template<class ElementVolumeVariables, class ElementFaceVariables>
    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const ElementFaceVariables& elemFaceVars,
                       const SubControlVolumeFace &scvf) const
    {
        auto source = NumEqVector(0.0);

#if !DIM_3D
            static const Scalar height = getParam<Scalar>("Problem.Height");
            static const Scalar factor = getParam<Scalar>("Problem.PseudoWallFractionFactor", 8.0);
            source[scvf.directionIndex()] = this->pseudo3DWallFriction(scvf, elemVolVars, elemFaceVars, height, factor);
#endif

        return source;
    }

    Scalar extrusionFactorAtPos(const GlobalPosition& pos) const
    { return extrusionFactor_; }


    // \}
    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary control volume.
     *
     * \param globalPos The position of the center of the finite volume
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;

        // set a fixed pressure at the inlet and outlet
        if (isOutlet_(globalPos) || isInlet_(globalPos))
        {
            values.setDirichlet(Indices::pressureIdx);
        }
        /*
        // would be boundary for slip boundary
        else if(isOnOutsideWall_(globalPos) )
        {
            values.setNeumann(Indices::pressureIdx);
        }
        */
        else //if (isOnWall_(globalPos))
        {
            values.setDirichlet(Indices::velocityXIdx);
            values.setDirichlet(Indices::velocityYIdx);
            if(dim == 3)
                values.setDirichlet(Indices::velocityZIdx);
        }
        return values;
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param globalPos The center of the finite volume which ought to be set.
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);

        if(isInlet_(globalPos))
            values[Indices::pressureIdx] = pressureInitial_ + deltaP_;

        if(isOutlet_(globalPos))
            values[Indices::pressureIdx] = pressureInitial_;

        return values;
    }
    
    // only necessary for slip boundary but doesnt work \TODO Check the boundary if its is possible
    PrimaryVariables neumannAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        return values;
    }

    // \}

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values(0.0);
        if(linearPressure_)
        {
            values[Indices::pressureIdx] = pressureInitial_ + deltaP_  - ( (pressureInitial_ + deltaP_) / this->fvGridGeometry().bBoxMax()[0] ) * globalPos[0];
        }
        return values;
    }

    //! Returns the analytical solution for the flux through the rectangular channel
    // \TODO not necessary anymore..
    Scalar analyticalFlux() const
    {
        const Scalar h = height_;
        const Scalar w = this->fvGridGeometry().bBoxMax()[1];
        const Scalar L = this->fvGridGeometry().bBoxMax()[0];

        const Scalar mu = nu_*rho_;

        return h*h*h * w * deltaP_ / (12*mu*L) * (1.0 - 0.630 * h/w);
    }

private:

    bool isInlet_(const GlobalPosition& globalPos) const
    {
        return globalPos[0] < eps_;
    }

    bool isOutlet_(const GlobalPosition& globalPos) const
    {
        return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_;
    }
    bool isOnOutsideWall_(const GlobalPosition& globalPos) const
    {
        return      globalPos[1]>this->fvGridGeometry().bBoxMax()[1] - eps_
                    ||   globalPos[1]<this->fvGridGeometry().bBoxMax()[1] + eps_
                    ;
    }

    Scalar eps_;
    Scalar deltaP_;
    Scalar pressureInitial_;
    bool linearPressure_;
    Scalar gradP_;
    Scalar extrusionFactor_;
    Scalar height_;
    Scalar rho_;
    Scalar nu_;
    
    Scalar porosity_;
    
    Scalar numDofs_x_;
    Scalar numDofs_y_;
    Scalar numDofs_z_;
 
    Scalar voxelSize_x_;
    Scalar voxelSize_y_;
    Scalar voxelSize_z_;
    
    Scalar areaTotal_;
    Scalar volumeTotal_;

};
} //end namespace

#endif
