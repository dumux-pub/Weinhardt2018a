/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Test for the cake grid creator
 */
#include <config.h>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/vtk.hh>

#include <dumux/common/parameters.hh>
#include <dumux/io/gridcreator.hh>
#include <dumux/io/subgridgridcreator.hh>
#include <dumux/discretization/methods.hh>

#include <dumux/io/container.hh>
#include <fstream>
/*!
 * \brief A method providing an () operator in order to select elements for a subgrid.
 */

typedef unsigned char BYTE;


bool isSolid(double row, double col)
{
    
    // Zu lesende Datei öffnen
	std::ifstream input("domain.txt");
	// Überprüfung ob Datei geöffnet wurde
	if(!input) {
		std::cerr << "Datei wurde nicht gefunden." << std::endl;
		return EXIT_FAILURE;
	}
    int darr[16];
	int index = 0;
	int wert;
    int num_el_x = 4;
    int num_el_y = 4;
    double length_x = 1.;
    double length_y = 1.;
    
    int pos_row = ( (length_x/num_el_x)/2 + row )* num_el_x;
    int pos_col = ( (length_y/num_el_y)/2 + col )* num_el_y;
    
    int pos = pos_row * pos_col -1;
	// Inhalt der Datei Zahl für Zahl einlesen
	
    /*
    while(input >> wert)
		// Weist einem Element des Arrays den zuletzt eingelesen Wert zu
		darr[index++] = wert;
	// Gibt alle Elemente des Arrays nebeneinader aus
    /*
	for(int ix = 0; ix != 16; ++ix)
		std::cout << darr[ix] << " ";
	std::cout << std::endl;
    
    std::cout << "row : " << row << " col: "<<col<<std::endl;
    std::cout << "pos_row : " << pos_row << " pos_col: "<<pos_col<<std::endl;
    std::cout << "Pos: " << pos << " darr = " << darr[pos] << std::endl;
    */
    return darr[pos];
}




template<class GlobalPosition>
class FlagsSelector
{
public:
    FlagsSelector(const GlobalPosition& center) : center_(center) {}

    //! Select all elements within a circle around a center point.
    int operator() (const auto& element) const
    {
        const auto x = element.geometry().center()[0];
        const auto y = element.geometry().center()[1];
        const double radius = 0.3;
        return isSolid(x, y);
    }
private:
    const GlobalPosition center_;
};


template<class GlobalPosition>
class CircleSelector
{
public:
    CircleSelector(const GlobalPosition& center) : center_(center) {}

    //! Select all elements within a circle around a center point.
    int operator() (const auto& element) const
    {
        const auto x = element.geometry().center()[0];
        const auto y = element.geometry().center()[1];
        const double radius = 0.3;
        return std::sqrt((x - center_[0])*(x - center_[0]) + (y - center_[1])*(y - center_[1])) < radius;
    }
private:
    const GlobalPosition center_;
};
/*
std::vector<BYTE> readFile(const char* filename)
{
    // open the file:
    std::streampos fileSize;
    std::ifstream file(filename, std::ios::binary);

    // get its size:
    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);

    // read the data:
    std::vector<BYTE> fileData(fileSize);
    file.read((char*) &fileData[0], fileSize);
    return fileData;
}
*/


template<typename Container>
Container readRawFileToContainer(const std::string& filename)
{
    // open the file:
    std::streampos fileSize;
    std::ifstream file(filename, std::ios::binary);

    // get its size:
    file.seekg(0, std::ios::end);
    fileSize = file.tellg();
    file.seekg(0, std::ios::beg);

    // read the data:
    std::vector<BYTE> fileData(fileSize);
    file.read((char*) &fileData[0], fileSize);
    return fileData;
}


/*
template<typename Container>
Container readRawFileToContainer(const std::string& filename)
{
// open the file:
    std::cout << "function called"<<std::endl;
    std::basic_ifstream<BYTE> file(filename, std::ios::binary);

    // read the data:
    return std::vector<BYTE>((std::istreambuf_iterator<BYTE>(file)),
                              std::istreambuf_iterator<BYTE>());
}
*/



int main(int argc, char** argv) try
{
    using namespace Dumux;

    // Initialize MPI, finalize is done automatically on exit.
    Dune::MPIHelper::instance(argc, argv);

    // First read parameters from input file.
    Dumux::Parameters::init(argc, argv);

    constexpr int dim = 2;
    using GlobalPosition = Dune::FieldVector<double, dim>;

    Dune::Timer timer;
    using HostGrid = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<double, dim> >;
    using HostGridCreator = GridCreatorImpl<HostGrid, DiscretizationMethod::none>;
    HostGridCreator::makeGrid();

    // Calculate the bounding box of the host grid view.
    GlobalPosition bBoxMin(std::numeric_limits<double>::max());
    GlobalPosition bBoxMax(std::numeric_limits<double>::min());
    
    
    const auto& gridView = HostGridCreator::grid().leafGridView();
    
    std::cout << "before reading raw file " << std::endl;
    //auto v = readRawFileToContainer<std::vector<unsigned int>>("50_50_1_circleMid.raw");
    auto v = readRawFileToContainer<std::vector<unsigned char>>("channel_50_50_100_2e-02_press.raw");
    
    std::cout << "size of vector : "<< v.size()<<std::endl;
    
    int size_x = 10;
    int size_y = 10;
    
    for(int i=0; i<size_x*size_y; i++)
    {
        std::cout << v[i] << " ";
        if((i+1)%10 ==0 )
            std::cout<<std::endl;
    }
    std::cout << "\nafter reading raw file " << std::endl;
    
    auto elementSelector = [&gridView, &v](const auto& element)
    {
        auto index = gridView.indexSet().index(element);
        //std::cout << "index = " << index << std::end;
        return v[index];
    }; 
    
    
    for (const auto& vertex : vertices(HostGridCreator::grid().leafGridView()))
    {
        for (int i=0; i<dim; i++)
        {
            using std::min;
            using std::max;
            bBoxMin[i] = min(bBoxMin[i], vertex.geometry().corner(0)[i]);
            bBoxMax[i] = max(bBoxMax[i], vertex.geometry().corner(0)[i]);
        }
    }

    // Get the center of the hostgrid's domain.
    const GlobalPosition center{bBoxMin[0]+0.5*bBoxMax[0], bBoxMin[1]+0.5*bBoxMax[1]};

    // Select all elements right of the center.
    auto elementSelectorOne = [&center](const auto& element)
    {
        return element.geometry().center()[0] > center[0];
    };

    // Select all elements left of the center.
    auto elementSelectorTwo = [&center](const auto& element)
    {
        return element.geometry().center()[0] < center[0];
    };

    // Select all elements within a circle around the center.
    // Instead of a lambda, we use a class providing an () operator.
    // Of course, a lambda would be possible here, too.
    FlagsSelector<GlobalPosition> elementSelectorThree(center);

    // Create three different subgrids from the same hostgrid.
    auto subgridPtrOne = SubgridGridCreator<HostGrid>::makeGrid(HostGridCreator::grid(), elementSelector, "SubGridOne");
    auto subgridPtrTwo = SubgridGridCreator<HostGrid>::makeGrid(HostGridCreator::grid(), elementSelectorTwo, "SubGridTwo");
    auto subgridPtrThree = SubgridGridCreator<HostGrid>::makeGrid(HostGridCreator::grid(), elementSelectorThree, "SubGridThree");

    std::cout << "Constructing a host grid and three subgrids took "  << timer.elapsed() << " seconds.\n";

    // Write out the host grid and the subgrids.
    Dune::VTKWriter<HostGrid::LeafGridView> vtkWriter(HostGridCreator::grid().leafGridView());
    vtkWriter.write("hostgrid");

    return 0;
}
///////////////////////////////////////
//////// Error handler ////////////////
///////////////////////////////////////
catch (Dumux::ParameterException &e) {
    std::cerr << e << ". Abort!\n";
    return 1;
}
catch (Dune::Exception &e) {
    std::cerr << "Dune reported error: " << e << std::endl;
    return 3;
}
catch (...) {
    std::cerr << "Unknown exception thrown!\n";
    return 4;
}
